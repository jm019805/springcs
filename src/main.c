/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "main.h"
#include "stdio.h"
static void capFrameRate(long *then, float *remainder);

int maxTime;
int currentTime;
int gameOver;

int main(int argc, char *argv[])
{
	while (1){
	
	long then;
	float remainder;
	
	memset(&app, 0, sizeof(App));
	app.textureTail = &app.textureHead;
	
	initSDL();
	
	atexit(cleanup);
		
	initGame();
	
	initStage();
	
	gameOver = 0;
	
	then = SDL_GetTicks();
	
	remainder = 0;

	maxTime = 60000;
	currentTime = 0;
	
	
	
	while (!gameOver)
	{
		
		currentTime = SDL_GetTicks();
		if (currentTime > maxTime){
				gameOver = 1;
		}
		
		prepareScene();
		
		doInput();
		
		app.delegate.logic();
		
		app.delegate.draw();
		
		presentScene();
		
		capFrameRate(&then, &remainder);
	}
	
	// Run for 5 more seconds to display gameover text
	int tMinus5 = SDL_GetTicks() + 5000;
	while (1) if (SDL_GetTicks() > tMinus5) break;
	
	//END OF GAME LOOP
	SDL_Quit();
	}
	
}

static void capFrameRate(long *then, float *remainder)
{
	long wait, frameTime;
	
	wait = 16 + *remainder;
	
	*remainder -= (int)*remainder;
	
	frameTime = SDL_GetTicks() - *then;
	
	wait -= frameTime;
	
	if (wait < 1)
	{
		wait = 1;
	}
		
	SDL_Delay(wait);
	
	*remainder += 0.667;
	
	*then = SDL_GetTicks();
}
